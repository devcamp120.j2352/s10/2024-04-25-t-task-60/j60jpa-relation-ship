package com.devcamp.j60jparelationship.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.*;

@Entity
@Table(name = "region")
public class CRegion {
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long region_id;
	
	@Column(name = "region_code", unique = true)
	private String regionCode;
	
	@Column(name = "region_name")
	private String regionName;
	
	@ManyToOne
	@JsonIgnore
    @JoinColumn(name="country_id")
    private CCountry country;

	public CRegion() {
	}

	public CRegion(String regionCode, String regionName) {
		this.regionCode = regionCode;
		this.regionName = regionName;
	}

	public long getId() {
		return region_id;
	}

	public void setId(long region_id) {
		this.region_id = region_id;
	}

	public String getRegionCode() {
		return regionCode;
	}

	public void setRegionCode(String regionCode) {
		this.regionCode = regionCode;
	}

	public String getRegionName() {
		return regionName;
	}

	public void setRegionName(String regionName) {
		this.regionName = regionName;
	}

	/**
	 * @return the country
	 */
	public CCountry getCountry() {
		return country;
	}

	/**
	 * @param country the country to set
	 */
	public void setCountry(CCountry country) {
		this.country = country;
	}
}

