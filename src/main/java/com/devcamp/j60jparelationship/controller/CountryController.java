package com.devcamp.j60jparelationship.controller;
import java.util.*;

import com.devcamp.j60jparelationship.model.*;
import com.devcamp.j60jparelationship.repository.*;
import com.devcamp.j60jparelationship.service.CountryService;
import com.devcamp.j60jparelationship.service.RegionService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/")
@CrossOrigin(value = "*", maxAge = -1)
public class CountryController {
    @Autowired
    private CountryService countryService;
    @Autowired
    private RegionService regionService;

    //lấy danh sách country /countries
    @GetMapping("/countries")
	public ResponseEntity<List<CCountry>> getAllCountries() {
		try {
			return new ResponseEntity<>(countryService.getAllCountries(), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
    @GetMapping("/countries2")
	public ResponseEntity<List<CCountry>> getCountryByName(@RequestParam(value = "countryName") String countryName) {
		try {
			return new ResponseEntity<>(countryService.getAllCountries(countryName), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}	
    // lấy danh sách regions truyền vào country code /regions?countryCode=...
    @GetMapping("/regions")
	public ResponseEntity<Set<CRegion>> getRegionsByCountryCode(@RequestParam(value = "countryCode") String countryCode) {
		try {
			Set<CRegion> regions = countryService.getRegionsByCountryCode(countryCode);
			if (regions != null) {
				return new ResponseEntity<>(regions, HttpStatus.OK);
			} else {
				return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

    @GetMapping("/regions-all")
	public ResponseEntity<List<CRegion>> getRegionsAll() {
        try {
			return new ResponseEntity<>(regionService.getAllRegions(), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

    /****************************************************************
     * Cách không dùng service class
     */
    @Autowired
	private ICountryRepository pCountryRepository;
    @Autowired
	private IRegionRepository pRegionRepository;
    @GetMapping("/allRegions")
	public ResponseEntity<List<CRegion>> getAllRegions() {
		try {
			List<CRegion> pRegions = new ArrayList<CRegion>();
			pRegionRepository.findAll().forEach(pRegions::add);
			return new ResponseEntity<>(pRegions, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
    @GetMapping("/regionsByid")
	public ResponseEntity<Set<CRegion>> getRegionsByCountryId(@RequestParam(value = "countryId") Long id) {
		try {
			Optional<CCountry> oCountry = pCountryRepository.findById(id);
			if (oCountry.isPresent()) {
				CCountry vCountry = oCountry.get();
				return new ResponseEntity<>(vCountry.getRegions(), HttpStatus.OK);
			} else {
				return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
